package it.rfidlab.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import static android.bluetooth.BluetoothAdapter.ACTION_SCAN_MODE_CHANGED;
import static android.bluetooth.BluetoothAdapter.EXTRA_PREVIOUS_SCAN_MODE;
import static android.bluetooth.BluetoothAdapter.EXTRA_SCAN_MODE;
import static android.bluetooth.BluetoothAdapter.SCAN_MODE_CONNECTABLE;
import static android.bluetooth.BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE;
import static android.bluetooth.BluetoothAdapter.SCAN_MODE_NONE;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private TextView remoteAddress;
    private TextView sent;
    private TextView received;
    private MainActivity activity = this;

    private BluetoothServerSocket server;
    private BluetoothSocket client = null;
    private InputStream i;
    private OutputStream o;
    private BluetoothAdapter adapter;

    private static final int REQUEST_ENABLE_BT = 100;
    private static final int REQUEST_ENABLE_DISCOVERABLE = 200;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        remoteAddress = findViewById(R.id.remote_address);
        sent = findViewById(R.id.sent);
        received = findViewById(R.id.received);

        TextView localAddress = findViewById(R.id.local_address);
        TextView localName = findViewById(R.id.local_name);
        generateName();
        localName.setText(String.format("Local name: %s", name));

        adapter = BluetoothAdapter.getDefaultAdapter();

        String macAddress = android.provider.Settings.Secure.getString(getContentResolver(), "bluetooth_address");
        Log.e("MAC", macAddress);
        localAddress.setText(String.format(Locale.UK, "Local address: %s", macAddress));

        if(activateBluetooth()) {
            discardPreviousDiscovering();
            enableDiscoverability();
        }

        registerReceiver(scanModeReceiver, new IntentFilter(ACTION_SCAN_MODE_CHANGED));
    }

    private void generateName(){
        String code = String.format(Locale.ITALIAN, "LSW_%04d", new Random().nextInt(9999));
        this.name = code;
    }

    private Thread bluetoothThread;
    
    private BroadcastReceiver scanModeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(ACTION_SCAN_MODE_CHANGED.equals(intent.getAction())){
                if(intent.hasExtra(EXTRA_SCAN_MODE)) {
                    switch (intent.getIntExtra(EXTRA_SCAN_MODE, 666)){
                        case SCAN_MODE_NONE:
                            Log.d(TAG, "onReceive: EXTRA_SCAN_MODE: SCAN_MODE_NONE: " + SCAN_MODE_NONE);
                            break;

                        case SCAN_MODE_CONNECTABLE:
                            Log.d(TAG, "onReceive: EXTRA_SCAN_MODE: SCAN_MODE_CONNECTABLE: " + SCAN_MODE_CONNECTABLE);
                            break;

                        case SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                            Log.d(TAG, "onReceive: EXTRA_SCAN_MODE: SCAN_MODE_CONNECTABLE_DISCOVERABLE: " + SCAN_MODE_CONNECTABLE_DISCOVERABLE);
                            break;

                        default:
                            Log.d(TAG, "onReceive: EXTRA_SCAN_MODE: UNKNOWN: " + 666);
                            break;
                    }

                } else {
                    Log.d(TAG, "onReceive: no SCAN_MODE" );
                }

                if(intent.hasExtra(EXTRA_PREVIOUS_SCAN_MODE)) {
                    switch (intent.getIntExtra(EXTRA_PREVIOUS_SCAN_MODE, 666)){
                        case SCAN_MODE_NONE:
                            Log.d(TAG, "onReceive: EXTRA_PREVIOUS_SCAN_MODE: SCAN_MODE_NONE: " + SCAN_MODE_NONE);
                            break;

                        case SCAN_MODE_CONNECTABLE:
                            Log.d(TAG, "onReceive: EXTRA_PREVIOUS_SCAN_MODE: SCAN_MODE_CONNECTABLE: " + SCAN_MODE_CONNECTABLE);
                            break;

                        case SCAN_MODE_CONNECTABLE_DISCOVERABLE:
                            Log.d(TAG, "onReceive: EXTRA_PREVIOUS_SCAN_MODE: SCAN_MODE_CONNECTABLE_DISCOVERABLE: " + SCAN_MODE_CONNECTABLE_DISCOVERABLE);
                            break;

                        default:
                            Log.d(TAG, "onReceive: EXTRA_PREVIOUS_SCAN_MODE: UNKNOWN: " + 666);
                            break;
                    }

                } else {
                    Log.d(TAG, "onReceive: no EXTRA_PREVIOUS_SCAN_MODE" );
                }
            }
        }
    };

    private void startBluetoothActivities(){

        bluetoothThread = new Thread(){
            @Override
            public void run() {
                super.run();
                Looper.prepare();
                int byteRead = 0;

                while (byteRead <= 0) {
                    try {
                        //creo la connessione
                        UUID uuid = UUID.fromString("00000003-0000-1000-8000-00805f9b34fb");
                        server = adapter.listenUsingInsecureRfcommWithServiceRecord("BT_SERVER", uuid);
                        adapter.setName(name);
                        Log.d(TAG, "run: accepting...");

                        client = server.accept(); //quest'operazione è bloccante
                        client.getRemoteDevice().setPin(new byte[]{0, 0, 0, 1});

                        //remote address text view
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                remoteAddress.setText(String.format("Remote address: %s", client.getRemoteDevice().getAddress()));
                            }
                        });

                        //read
                        i = client.getInputStream();
                        o = client.getOutputStream();

                        int l = i.available();
                        while (l <= 0) {
                            l = i.available();
                        }


                        final byte[] bytes = new byte[l];
                        byteRead = i.read(bytes);
                        Log.d(TAG, "run: byteRead: " + byteRead);
                        Log.e(TAG, new String(bytes));

                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                received.setText(String.format("Received: %s", new String(bytes)));
                                Log.e(TAG, new String(bytes));
                            }
                        });

                        //send
                        final JSONObject j = new JSONObject();
                        j.put("error", "ok");
                        client.getOutputStream().write(j.toString().getBytes());
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                sent.setText(String.format("Sent: %s", j.toString()));
                            }
                        });

                        Log.e("Sent", j.toString());

                    } catch (Exception e) {
                        //handleException("onActivityResult", e);
                        Log.e(TAG, "exception: " + e.getMessage());
                        SystemClock.sleep(1500);
                    }

                }

                closeAll();
                adapter.disable();
            }
        };

        bluetoothThread.start();
    }

    private boolean activateBluetooth() {

        if(!adapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
            return false;
        }

        return true;
    }

    private void closeAll(){
        if(i!=null){
            try {
                i.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if(o!=null){
            try {
                o.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if(client!=null){
            try {
                client.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if(server!=null){
            try {
                server.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if(adapter != null)
            adapter.disable();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterReceiver(scanModeReceiver);

        if(bluetoothThread != null)
            bluetoothThread.interrupt();

        closeAll();
    }

    private void handleException(String tag, Exception e) {
        Log.e(tag, e.getMessage());
        e.printStackTrace();
    }

    private void discardPreviousDiscovering(){
        Log.e(TAG,"Check previous disconvering...");
        if(adapter.isDiscovering())
            adapter.cancelDiscovery();
        while (adapter.isDiscovering());
        Log.e(TAG,"Disconvering: " + adapter.isDiscovering());
    }

    private void enableDiscoverability(){
        Intent discoverableIntent =
                new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 3600);
        startActivityForResult(discoverableIntent, REQUEST_ENABLE_DISCOVERABLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_ENABLE_BT){
            if(resultCode == RESULT_OK){
                enableDiscoverability();
            }
        } else if(requestCode == REQUEST_ENABLE_DISCOVERABLE){
            if(resultCode == 3600){
                Log.d(TAG, "onActivityResult: discoverable enabled for 3600 seconds");
                startBluetoothActivities();
            } else {
                Log.e(TAG, "onActivityResult: discoverable not enabled");
            }
        }
    }
}
